import React, { Component } from 'react';

export default class Quotes extends Component {

    render() {
        return (
            <div>
                <h1 className="text-center text-white">Market rates</h1>
                <div className="row">
                    <div className="col-xl-5">
                        <hr/>
                        <h4 className="text-white">DAI</h4>
                        <table className="table table-responsive-lg text-white">
                            <thead className="thead-dark">
                                <tr>
                                    <th></th>
                                    <th>Buy</th>
                                    <th>Total buy</th>
                                    <th>Sell</th>
                                    <th>Total sell</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="text-white">exchange</td>
                                    <td className="text-white">buy</td>
                                    <td className="text-white">totol buy</td>
                                    <td className="text-white">sell</td>
                                    <td className="text-white">total sell</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col-xl-5">                        
                    <hr/>
                    <h4 className="text-white">Ethereum</h4>
                    <table className="table table-responsive-lg text-white">
                        <thead className="thead-dark">
                            <tr>
                                <th></th>
                                <th>Buy</th>
                                <th>Total buy</th>
                                <th>Sell</th>
                                <th>Total sell</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="text-white">exchange</td>
                                <td className="text-white">buy</td>
                                <td className="text-white">totol buy</td>
                                <td className="text-white">sell</td>
                                <td className="text-white">total sell</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xl-5">
                        <hr/>
                        <h4 className="text-white">Bitcoin</h4>
                        <table className="table table-responsive-lg text-white">
                            <thead className="thead-dark">
                                <tr>
                                    <th></th>
                                    <th>Buy</th>
                                    <th>Total buy</th>
                                    <th>Sell</th>
                                    <th>Total sell</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="text-white">exchange</td>
                                    <td className="text-white">buy</td>
                                    <td className="text-white">totol buy</td>
                                    <td className="text-white">sell</td>
                                    <td className="text-white">total sell</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}