import React, { Component } from 'react';

import Calculator from '../components/home/Calculator';
import Results from '../components/home/Results';
import ArrowUp from '../components/home/ArrowUp';

export default class Home extends Component {

    constructor() {
        super();
        this.state = {
            data: {},
            loading: false,
            vars: {}
        };
    }

    setData = (data, vars) => {
        this.setState({ data: data, loading: true, vars: vars });
    }

    render() {
        return (
            <div>
                <Calculator handleResults={this.setData}/>     
                {
                    this.state.loading ?
                            <Results data={this.state.data} vars={this.state.vars}/>
                        : <div/>
                }
                <ArrowUp/>
            </div>
        )
    }
}
